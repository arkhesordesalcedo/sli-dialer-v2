<?php

require dirname(__FILE__) . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

use SliCallCenter\Helpers\Logging;
use SliCallCenter\Helpers\Validate;
use SliCallCenter\Channels\Queue;

$loop = React\EventLoop\Factory::create();

$context = new React\ZMQ\Context($loop);
$pusher = $context->getSocket(\ZMQ::SOCKET_PULL);
$pusher->bind(getenv('SLI_CALLCENTER_ZMQ_CONNECTION_2'));
$pusher->on('message', function($message){
	if(!$validatedMessage = Validate::incoming($message)) {
        return false;
    }

    Logging::write('PUSHER_2[DB SAVING]: Data saving on queue: ' . $validatedMessage['data'][0]);
    
    Queue::save($validatedMessage['data'][0], $validatedMessage['data'][1]);
});

$loop->run();
