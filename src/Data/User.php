<?php

namespace SliCallCenter\Data;

use Carbon\Carbon;
use Predis\Collection\Iterator;
use Ratchet\ConnectionInterface;
use SliCallCenter\Helpers\Utils;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Pbx\ActionHandler;
use SliCallCenter\Connectors\RedisClient;

class User
{
	/**
	 * Checks if a user is allowed to make calls and if extension is available
	 * @param  string $userGuid guid of the caller
	 * @return boolean            returns true if allowed and available
	 */
	public static function allowed($userGuid)
	{
		$extension = self::extension($userGuid);

		return Extension::allowed($extension) && Extension::available($extension);
	}
	
	/**
	 * Authorize a given connection 
	 * @param  ConnectionInterface $conn     socket connection object
	 * @param  string              $username users username
	 * @return boolean                       returns true if authorization is successful
	 */
	public static function authorize(ConnectionInterface $conn, $username)
	{
		return (boolean) RedisClient::connection()->set("slicc_authorized_user_name:$username", $conn->resourceId);
	}

	public static function track($type, $userGuid, $callGuid, $lead)
	{
		$extension = User::extension($userGuid);

		RedisClient::connection()->set("slicc_user_lead_$type:$userGuid", $lead['data']['id']);

		RedisClient::connection()->set("slicc_user_call_$type:$userGuid", $callGuid);

		RedisClient::connection()->lpush("slicc_extension_call_type:$extension", $type); // temp put here
	}

	public static function extension($userGuid)
	{
		return RedisClient::connection()->get("slicc_user_extension:$userGuid");
	}

	public static function guid($extension)
	{
		return RedisClient::connection()->get("slicc_extension_user:$extension");
	}

	public static function licensed($userGuid)
	{
		$states = [];

		foreach (new Iterator\SetKey(RedisClient::connection(), "slicc_user_states:$userGuid") as $state) {
		    array_push($states, $state);
		}

		return $states;
	}

	public static function name($userGuid)
	{
		return RedisClient::connection()->hget('slicc_user_info:' . $userGuid, 'first_name') . ' ' . RedisClient::connection()->hget('slicc_user_info:' . $userGuid, 'last_name');
	}

	public static function getHash($username)
	{
		return RedisClient::connection()->get("slicc_user_hash:$username");
	}

	public static function getHashEncoded($username)
	{
		return RedisClient::connection()->get("slicc_user_hash_encoded:$username");
	}

	public static function calls($userGuid, $type = 'outbound')
	{
		return RedisClient::connection()->get("slicc_user_call_$type:$userGuid");
	}

	public static function leads($userGuid, $type = 'outbound')
	{
		return RedisClient::connection()->get("slicc_user_lead_$type:$userGuid");
	}

	public static function call($data, $lead, $type = 'outbound', $callGuid = null)
	{
		// TODO: refactor this shit
		// generate guid if null
        $callGuid = $callGuid ?: Utils::generateGUID();

        // track calls and lead for user to process
        self::track($type, $data->get('userGuid'), $callGuid, $lead);
        // process and update lead
        Lead::process($lead, $data->get('userGuid'));
        // register a call to crm if outbound
        if ($type == 'outbound') {
        	Call::register($data->get('userGuid'), $callGuid, $lead);
        }

        // dial
        ActionHandler::call(
            User::extension($data->get('userGuid')), 
            $lead['phoneNumber']
        );

        // reply immediately and process data later
        $data->reply([
        	'data' => [
        		'lead' => $lead['data']
        	]
        ]);

		return true;
	}

	public static function callback($userGuid)
	{
		$now = Carbon::now('UTC')->timestamp;

		foreach (new Iterator\SortedSetKey(RedisClient::connection(), "slicc_user_callbacks:$userGuid") as $callGuid => $timestamp) {
		    if ($now > $timestamp) {
		    	Logging::write('DATA_USER[callback]: Callback due: ' . $timestamp);

		    	return $callGuid;
		    }
		}

		return false;
	}

	// public static function recentCalls($userGuid, $callbacks = false)
	// {
	// 	if ($callbacks) {
	// 		$callGuids = RedisClient::connection()->zrange('slicc_user_callbacks:' . $userGuid, 0, 15);
	// 	} else {
	// 		$callGuids = RedisClient::connection()->zrevrange('slicc_user_calls:' . $userGuid, 0, 15);
	// 	}

	// 	return Utils::transform($callGuids, ['date_start', 'date_entered'], 'slicc_call_info:');
	// }

	// public static function recentLeads($userGuid)
	// {
	// 	$leadGuids = RedisClient::connection()->zrevrange('slicc_user_leads:' . $userGuid, 0, 15);

	// 	return Utils::transform($leadGuids, ['date_modified'], 'slicc_lead_info:');
	// }

	public static function incoming($userGuid)
	{
		return RedisClient::connection()->get('slicc_inbound_request:' . $userGuid);
	}

	public static function catched($userGuid)
	{
		return RedisClient::connection()->get('slicc_inbound_catched:' . $userGuid);
	}

	public static function inbound($data, $lead, $userGuid, $feedback)
	{
		if (self::incoming($userGuid)) {
			RedisClient::connection()->del('slicc_inbound_request:' . $userGuid);

			RedisClient::connection()->set('slicc_inbound_catched:' . $userGuid, $lead['id']);

			$feedback = Lead::inbound($lead['phone_mobile'], $feedback);

	        $data->reply($feedback, null, 'getCall');

	        return true;
		}

		return false;
	}
}