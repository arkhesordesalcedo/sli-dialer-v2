<?php

namespace SliCallCenter\Data;

use Carbon\Carbon;
use Predis\Collection\Iterator;
use SliCallCenter\Connectors\RedisClient;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Helpers\Timezone;
use SliCallCenter\Helpers\Operation;
use SliCallCenter\Helpers\Utils;

class Lead extends Model
{
	public static function dispose($data)
	{
		self::set($data->get('leadGuid'), 'status', $data->get('dispositionValue'));

		self::status($data);

        $extension = User::extension($data->get('userGuid'));

        $type = Extension::callType($extension);

        RedisClient::connection()->srem('slicc_lead_processing', $data->get('leadGuid'));

        RedisClient::connection()->del("slicc_user_lead_$type:" . $data->get('userGuid'));

        RedisClient::connection()->lpop("slicc_extension_call_type:$extension");

        RedisClient::connection()->del("slicc_user_call_inbound:" . $data->get('userGuid'));

        RedisClient::connection()->del("slicc_inbound_catched:" . $data->get('userGuid'));

        RedisClient::connection()->set("slicc_extension_status:$extension", 0);

        self::update($data);

        return true;
	}

	public static function callback($data)
	{
		// TODO: refactor this shit
		if ($cbInfo = $data->get('cbInfo')) {
			$type = $data->get('dispositionValue') == 'Callback Scheduled' ? 'Callback' : $data->get('dispositionValue');
			
			$callGuid = Utils::generateGUID();

			$userGuid = $data->get('userGuid');

			$lead = self::transform($data->get('leadGuid'));

			// convert time to lead's timezone
			try {
		        $start = Carbon::createFromFormat('Y-m-d H:i:s', $cbInfo['start'], Timezone::get($lead['data']['primary_address_state']));
		    } catch (\Exception $exception) {
		        $start = Carbon::now(Timezone::get($lead['data']['primary_address_state']))->addDay();
		    }

			// convert time to UTC before saving to DB
			$start->timezone('UTC');

			Logging::write('DATA_LEAD[callback]: Callback start: ' . print_r($start, true));

			$reason = $cbInfo['reason'];

			if ($type == 'Call_Setup') {
				Call::appointment($userGuid, $callGuid, $lead, $start, $reason);
			} else {
				RedisClient::connection()->srem('slicc_lead_do_not_dial', $data->get('leadGuid'));
				
				RedisClient::connection()->zadd("slicc_user_callbacks:" . $userGuid, $start->timestamp, $callGuid);

				RedisClient::connection()->sadd("slicc_lead_callbacks" , $data->get('leadGuid'));

	            RedisClient::connection()->set("slicc_call_lead_callback:" . $callGuid, $data->get('leadGuid'));
            
				Call::planner($userGuid, $callGuid, $lead, $start, $reason, $type);
			}
		}
	}

	public static function inbound($phone, $feedback = null)
	{
		$feedback['data']['from']['inboundNumber'] = $phone;

        $feedback['data']['from']['DID'] = RedisClient::connection()->get("slicc_exten_did:" . $phone);
        
        $feedback['data']['from']['calleridname'] = RedisClient::connection()->get("slicc_exten_callerid:" . $phone);

        return $feedback;
	}

	public static function status($data)
	{
		// validate disposition first
        if ($data->get('dispositionValue') == 'Voicemail') {
        	self::state($data->get('leadGuid'), 'sadd');

        	RedisClient::connection()->srem('slicc_lead_do_not_dial', $data->get('leadGuid'));
        	
        	RedisClient::connection()->srem('slicc_lead_callbacks', $data->get('leadGuid'));
        	// set lead to waiting list
        	self::delay($data->get('leadGuid'));
        } else {
        	// removing from callable by state
        	self::state($data->get('leadGuid'), 'srem');
        	// set do not dial so it won't go back to the queue no matter what
        	RedisClient::connection()->sadd('slicc_lead_do_not_dial', $data->get('leadGuid'));
        	// check if this is a callback 
        	self::callback($data);
        }
	}

	public static function state($leadGuid, $command)
	{
		return RedisClient::connection()->{$command}('slicc_lead_callable_by_state:' . str_replace(' ', '_', self::get($leadGuid, 'primary_address_state')), $leadGuid);
	}

	public static function delay($leadGuid)
	{
		RedisClient::connection()->sadd('slicc_lead_waiting', $leadGuid);

		return self::interval($leadGuid);
	}

	private static function interval($leadGuid)
	{
        $leadDials = self::get($leadGuid, 'total_dials');

        foreach (Operation::sequence() as $dials => $delay) {
            if ($leadDials <= $dials) {
            	$interval = Carbon::now('America/Toronto')->addMinutes($delay);

                RedisClient::connection()->zadd('slicc_lead_waiting_timer', Operation::calculate($interval->timestamp), $leadGuid);

                break;
            }
        }
	}

	private static function check()
	{
		$now = Carbon::now('America/Toronto')->timestamp;

		foreach (new Iterator\SortedSetKey(RedisClient::connection(), 'slicc_lead_waiting_timer') as $leadGuid => $timestamp) {
		    if ($now > $timestamp) {
		    	Logging::write('DATA_LEAD[check]: Removing lead from waiting list: ' . $leadGuid);

                RedisClient::connection()->zrem("slicc_lead_waiting_timer", $leadGuid);

                RedisClient::connection()->srem("slicc_lead_waiting",  $leadGuid);
		    }
		}
	}

	public static function duplicates($phone)
	{
		// TODO: check for duplicates here
	}

	public static function age($leadGuid)
	{
		return Carbon::parse(RedisClient::connection()->hget("slicc_lead_info:" . $leadGuid, 'date_entered'))->diffInDays(Carbon::now('UTC'));
	}

	public static function set($leadGuid, $field, $value)
	{
		return RedisClient::connection()->hset("slicc_lead_info:" . $leadGuid, $field, $value);
	}

	public static function get($leadGuid, $field)
	{
		return RedisClient::connection()->hget("slicc_lead_info:" . $leadGuid, $field);
	}

	public static function provide($userGuid)
	{	
		Logging::write('DATA_LEAD[provide]: Searching lead for: ' . $userGuid);
		// check if we can check callbacks and incoming calls here
		if ($licensed = User::licensed($userGuid)) {
			$states = array_map(function($state) {
				if (Timezone::allowed($state)) {
					return "slicc_lead_callable_by_state:$state";
				}
			}, $licensed);

			if ($leadGuid = self::filter($userGuid, $states)) {
				return self::transform($leadGuid[0]);
			}
		}

		return false;
	}

	public static function process($lead, $userGuid)
	{
		RedisClient::connection()->sadd('slicc_lead_processing', $lead['data']['id']);

		RedisClient::connection()->hincrby("slicc_lead_info:" . $lead['data']['id'], 'total_dials', 1);

		Logging::write('DATA_LEAD[process]: Processing lead: ' . $lead['data']['id']);

		self::set($lead['data']['id'], 'date_modified', Carbon::now('UTC')->toDateTimeString());

		self::push('Leads', [
			'id' => $lead['data']['id'],
            'status' => 'Processing',
            'modified_user_id' => $userGuid,
            'update_modified_by' => false,
		]);

		return true;
	}

	public static function find($leadGuid)
	{
		$lead = RedisClient::connection()->hgetall("slicc_lead_info:$leadGuid");

		unset($lead['additional_notes_c']); // temporary

		return $lead;
	}

	public static function transform($leadGuid)
	{
		if ($lead = self::find($leadGuid)) {

			unset($lead['additional_notes_c']); // temporary

			return [
				'phoneNumber' => self::phone($lead),
				'data' => $lead
			];
		}

		return false;
	}

	public static function phone(array $lead)
	{
        $phoneNumbers = array(
            $lead['phone_mobile'],
            $lead['phone_home'],
            $lead['phone_work'],
            $lead['phone_other']      
        );
        
        foreach ($phoneNumbers as $number) {
            if ((int) $number) {
                return $number;
            }
        }

        return false;
	}

	private static function filter($userGuid, $states)
	{
		// check if lead waiting time is expired already
		self::check();

		RedisClient::connection()->transaction(function($tx) use ($userGuid, $states) {
			$tx->multi();

			$tx->sunionstore("slicc_lead_callable_by_user:$userGuid", $states);

			$tx->sdiffstore(
				"slicc_lead_callable_by_user_filtered:$userGuid", 
				"slicc_lead_callable_by_user:$userGuid", 
				'slicc_lead_do_not_dial', 
				'slicc_lead_callbacks', 
				'slicc_lead_waiting', 
				'slicc_lead_processing');

			$tx->zinterstore(
				"slicc_lead_callable_by_user_sorted:$userGuid", 
				2, 
				"slicc_lead_callable_by_user_filtered:$userGuid",
				'slicc_lead_sorted_by_date_entered',
				[
					'aggregate' => 'max',
					'weights' => [1, 1]
				]
			);
		});

		$leadGuid = RedisClient::connection()->zrevrange("slicc_lead_callable_by_user_sorted:$userGuid", 0, 1);

		RedisClient::connection()->del("slicc_lead_callable_by_user_sorted:$userGuid");

		RedisClient::connection()->del("slicc_lead_callable_by_user_filtered:$userGuid");

		RedisClient::connection()->del("slicc_lead_callable_by_user:$userGuid");

		return $leadGuid;
	}

	public static function create($leadGuid, $userGuid, $exten, $did)
	{
		RedisClient::connection()->sadd("slicc_lead_inquiry", $exten);

		RedisClient::connection()->set("slicc_phone_lead:" . $exten, $leadGuid);

		// TODO: try and check for duplicate phone numbers first
		$data = [
			'id' => $leadGuid,
			'new_with_id' => 1,
	    	'status' => 'Inquiry',
	    	'last_name' => $exten,
	    	'birthdate' => '',
	    	'phone_work' => '',
	        'phone_home' => '',
	    	'first_name' => '',
	    	'total_dials' => 0,
	    	'phone_other' => '',
	    	'phone_mobile' => $exten,
	    	'date_entered' => Carbon::now('UTC'),
	    	'date_modified' => Carbon::now('UTC'),
	    	'opportunity_name' => '',
	        'opportunity_amount' => '',
	        'primary_address_state' => 'Ontario',
	        'primary_address_street' => '',
	        'primary_address_city' => '',
	        'primary_address_postalcode' => '',
	        'primary_address_country' => '',
	    	'gender_c' => '',
	    	'smoker_c' => '',
	    	'marketing_id_c' => $did . '_' . $exten . '_inquiry',
	    	'website_code_c' => '',
	    	'coverage_requested_c' => '',
	    	'assigned_user_id' => $userGuid
		];

		RedisClient::connection()->hmset("slicc_lead_info:$leadGuid", $data);

		self::register($data, $userGuid);

		return self::transform($leadGuid);
	}

	public static function modify($data)
	{
		Logging::write('DATA_LEAD[modify]: Modifying lead: ' . $data['id']);

		// remove state in case changed
		self::state($data['id'], 'srem');

		RedisClient::connection()->hmset('slicc_lead_info:' . $data['id'], $data);
		// re add callable by state
		self::state($data['id'], 'sadd');

		self::set($data['id'], 'date_modified', Carbon::now('UTC')->toDateTimeString());

		self::push('Leads', $data);

		return true;
	}

	public static function update($data)
	{
		Logging::write('DATA_LEAD[update]: Updating lead: ' . $data->get('leadGuid'));

		self::set($data->get('leadGuid'), 'date_modified', Carbon::now('UTC')->toDateTimeString());

		self::push('Leads', [
			'id' => $data->get('leadGuid'),
            'status' => $data->get('dispositionValue'),
            'last_caller_guid_c' => $data->get('userGuid'),
            'dispo_notes' => $data->get('notes'),
            'modified_user_id' => $data->get('userGuid'),
            'update_modified_by' => false,
            'total_dials' => self::get($data->get('leadGuid'), 'total_dials'),
            'last_called_timestamp_c' => Carbon::now('UTC')->timestamp
		]);

		return true;
	}

	public static function register($data, $userGuid)
	{
		Logging::write('DATA_LEAD[register]: Register lead: ' . $data['id']);

		self::push('Leads', $data);

		return true;
	}
}
