<?php

namespace SliCallCenter\Data;

use Ratchet\ConnectionInterface;
use SliCallCenter\Connectors\RedisClient;

class Connection
{
	public static function allowed(ConnectionInterface $conn)
	{
		return (boolean) RedisClient::connection()->sismember('slicc_authorized_connection', $conn->resourceId);
	}

	public static function authorize(ConnectionInterface $conn)
	{
		return RedisClient::connection()->sadd('slicc_authorized_connection', $conn->resourceId);
	}

	public static function remove(ConnectionInterface $conn, $resouceId = null)
	{
		return RedisClient::connection()->srem('slicc_authorized_connection', $resouceId ?: $conn->resourceId);
	}
}