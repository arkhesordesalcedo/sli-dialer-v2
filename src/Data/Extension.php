<?php

namespace SliCallCenter\Data;

use Predis\Collection\Iterator;
use SliCallCenter\Connectors\RedisClient;

/**
 * Extension related actions/queries wrapper
 */
class Extension
{
	/**
	 * Checks if an extension is allowed to make calls or take calls
	 * @param  string $extension user extension number
	 * @return boolean           returns true if allowed
	 */
	public static function allowed($extension)
	{
		return (boolean) RedisClient::connection()->sismember('slicc_extensions_allowed', $extension);
	}

	/**
	 * Checks if extension is idle, thus it is currently available to call/receive call
	 * @param  string $extension user extension number
	 * @return boolean returns true if extension status is 0
	 */
	public static function available($extension)
	{
		return (boolean) !RedisClient::connection()->get("slicc_extension_status:$extension");
	}

	/**
	 * Set extension status
	 * @param  string $extension user extension number
	 * @param  string $status    user current status
	 * @return boolean           returns false if not allowed or if insertion fails, otherwise returns true
	 */
	public static function status($extension, $status)
	{
		return !self::allowed($extension) ?: RedisClient::connection()->set("slicc_extension_status:$extension", $status);
	}

	/**
	 * Returns all allowed extensions
	 * @return array lists of all extensions allowed
	 */
	public static function all()
	{
		$extensions = [];

		foreach (new Iterator\SetKey(RedisClient::connection(), 'slicc_extensions_allowed') as $extension) {
		    array_push($extensions, $extension);
		}

		return $extensions;
	}

	public static function callType($extension)
	{
		if ($type = RedisClient::connection()->lrange("slicc_extension_call_type:$extension", 0, 0)) {
			return $type[0];
		}

		$callTypes = ['inbound', 'outbound', 'manual', 'callback', 'incoming'];

		$userGuid = User::guid($extension);

		foreach ($callTypes as $type) {
			if (RedisClient::connection()->get("slicc_user_lead_$type:$userGuid")) {
				return $type;
			}
		}

		return false;
	}

	public static function endCall($userGuid, $extension)
	{
		$callTypes = ['inbound', 'outbound', 'manual', 'callback', 'incoming'];

		foreach ($callTypes as $type) {
			// TODO: figure out a way to update calls that AMI fails to track
			RedisClient::connection()->del("slicc_extension_call_$type:" . $userGuid);

			RedisClient::connection()->del("slicc_extension_call_type:" . $extension);

			RedisClient::connection()->del("slicc_user_call_$type:" . $userGuid);
		}

		RedisClient::connection()->set("slicc_extension_status:$extension", 0);

		RedisClient::connection()->del("slicc_user_call_inbound:" . $userGuid);
     
        RedisClient::connection()->del("slicc_inbound_catched:" . $userGuid);

		return true;
	}
}