<?php

namespace SliCallCenter\Data;

use Carbon\Carbon;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Helpers\Utils;
use SliCallCenter\Connectors\RedisClient;
use SliCallCenter\Pbx\ActionHandler;

class Call extends Model
{
    // currently not in use, testing getting DID from caller id prefix set on asterisk
    public static function variables($event)
    {
        $listen = ['__FROM_DID', 'FROMEXTEN'];

        $channel = $event->getKey('channel');

        $variable = $event->getKey('variable');

        $value = $event->getKey('value');

        if (RedisClient::connection()->get("slicc_new_exten_channel:$channel") && in_array($variable, $listen)) {
            if ($variable == 'FROMEXTEN' && $variable != 'unknown') {
                RedisClient::connection()->set("slicc_channel_exten:$channel", $value);
            }
            
            if ($variable == '__FROM_DID') {
                RedisClient::connection()->set("slicc_channel_did:$channel", $value);
            }

            $exten = RedisClient::connection()->get("slicc_channel_exten:$channel");

            $did = RedisClient::connection()->get("slicc_channel_did:$channel");

            if ($exten && $did) {

                RedisClient::connection()->set("slicc_exten_did:$exten", $did);

                RedisClient::connection()->expire("slicc_exten_did:$exten", 600);

                RedisClient::connection()->del("slicc_channel_exten:$channel");

                RedisClient::connection()->del("slicc_channel_did:$channel");

                Logging::write('DATA_CALL[variables]: New exten and did:' . $exten . ' = ' . $did);
            }

            return true;
        }

        return false;
    }

    public static function type($event, $extension)
    {
        Logging::write('DATA_CALL[type]: Dial channel event:' . print_r($event->getKey('channel'), true));

        $userGuid = User::guid($extension);
        // TODO: DIRTY!!!!
        if (strpos($event->getKey('channel'), 'Local/' . $extension . '@') !== false && strpos($event->getKey('destination'), 'SIP/' . $extension . '-') !== false) {
            Logging::write('DATA_CALL[type]: Call type for extension:' . $extension . ' = inbound');

            $callGuid = Utils::generateGUID();

            $exten = $event->getKey('calleridnum');

            $channel = $event->getKey('channel');

            if(strpos($event->getKey('calleridname'), '@') !== false) {
                $caller = explode('@', $event->getKey('calleridname'));
            } else {
                $caller = [$event->getKey('connectedlinenum'), $event->getKey('calleridname')];
            }

            RedisClient::connection()->set("slicc_exten_did:$exten", $caller[0]);

            RedisClient::connection()->set("slicc_exten_callerid:$exten", 'maybe ' . $caller[1]);

            RedisClient::connection()->set("slicc_inbound_request:" . User::guid($extension), $exten);

            RedisClient::connection()->set("slicc_user_call_inbound:" . User::guid($extension), $callGuid);

            $did = RedisClient::connection()->get("slicc_exten_did:$exten");

            Logging::write('DATA_CALL[type]: DID and Exten:' . $did . ' - ' . $exten);

            $userGuid = User::guid($extension);

            if (RedisClient::connection()->sismember("slicc_lead_inquiry", $exten)) {
                $leadGuid = RedisClient::connection()->get("slicc_phone_lead:" . $exten);

                $lead = Lead::transform($leadGuid);
            } else {
                $leadGuid = Utils::generateGUID();

                $lead = Lead::create($leadGuid, $userGuid, $exten, $did);
            }

            User::track('inbound', $userGuid, $callGuid, $lead);

            Lead::process($lead, $userGuid);

            Call::register($userGuid, $callGuid, $lead, $type = 'Inbound');

            return  'inbound';
        }

        if (strpos($event->getKey('destination'), 'SIP/' . $extension . '-') !== false) {
            Logging::write('DATA_CALL[type]: Call type for extension:' . $extension . ' = incoming');

            return  'incoming';
        }
        // TODO: fix this shit
        if ($callGuid = User::callback($userGuid)) {
            if ($callGuid = RedisClient::connection()->get("slicc_user_call_callback:" . $userGuid)) {
                Logging::write('DATA_CALL[type]: Call type for extension:' . $extension . ' = callback');

                return 'callback';
            }
        }

        return 'outbound';
    }

    public static function guid($type, $userGuid)
    {
        return RedisClient::connection()->get("slicc_user_call_$type:$userGuid");
    }

    public static function make($event, $extension)
    {
        $type = self::type($event, $extension);

        $userGuid = User::guid($extension);

        $callGuid = RedisClient::connection()->get("slicc_user_call_$type:$userGuid");

        self::reconnect($userGuid, $callGuid, $type); // checks if redial or user still on a call that was disconnected

        self::track($extension, $type, $event);
    }

    public static function hangup($event, $extension)
    {
        // check if normal clearing
        $userGuid = User::guid($extension);
        
        if ($extension && $event->getKey('cause') == 16) {
            $type = Extension::callType($extension);

            $callGuid = self::guid($type, $userGuid);

            self::untrack($extension, $type, $event);

            self::end($type, $extension, $callGuid, $userGuid, $event);
        }

        RedisClient::connection()->del("slicc_inbound_catched:$userGuid");

        RedisClient::connection()->del("slicc_inbound_request:$userGuid");
    }

    public static function timer($type, $extension)
    {
        return RedisClient::connection()->get("slicc_timer_call_$type:" . $extension) ?: Carbon::now('UTC')->timestamp;
    }

    public static function end($type, $extension, $callGuid, $userGuid, $event)
    {
        if ($callGuid) {
            $callStartTimestamp = self::timer($type, $extension);

            RedisClient::connection()->del("slicc_user_call_$type:" . $userGuid);

            RedisClient::connection()->del("slicc_timer_call_$type:" . $extension);

            $lead = Lead::transform(RedisClient::connection()->get("slicc_user_lead_$type:" . User::guid($extension)));
            // update calls in crm
            self::update($userGuid, $callGuid, $lead, $type, $event->getKey('uniqueid'), Carbon::createFromTimestampUTC($callStartTimestamp), Carbon::now('UTC'));
        }
    }

    public static function lead($callGuid)
    {
        return RedisClient::connection()->get("slicc_call_lead_callback:$callGuid");
    }

    public static function observe($event, $extension)
    {
        if ($extension) {
            $type = Extension::callType($extension);

            $userGuid = User::guid($extension);

            $callGuid = self::guid($type, $userGuid);

            if ($callGuid || $type == 'inbound') {
                self::bridge($extension, $type, $event);
            }
        }
    }

    public static function bridge($extension, $type, $event)
    {
        $command = $event->getKey('bridgestate') == 'Link' ? 'sadd' : 'srem';

        RedisClient::connection()->set("slicc_timer_call_$type:" . $extension, Carbon::now('UTC')->timestamp, 'NX');

        RedisClient::connection()->{$command}("slicc_bridge_channels_$type:" . $extension, $event->getKey('channel1'));

        RedisClient::connection()->{$command}("slicc_bridge_channels_$type:" . $extension, $event->getKey('channel2'));
    }

    public static function reconnect($userGuid, $callGuid, $type)
    {
        if (!$callGuid && $leadGuid = RedisClient::connection()->get("slicc_user_lead_$type:$userGuid")) { // check if there's a current call and lead still in processing
            $callGuid = Utils::generateGUID();

            RedisClient::connection()->set("slicc_user_call_$type:$userGuid", $callGuid);

            self::register($userGuid, $callGuid, Lead::transform($leadGuid), 'Manual');
        }
    }

    public static function track($extension, $type, $event)
    {
        RedisClient::connection()->set("slicc_extension_call_$type:" . $extension, $event->getKey('uniqueid'));

        RedisClient::connection()->set("slicc_call_extension_$type:" . $event->getKey('uniqueid'), $extension);

        // RedisClient::connection()->lpush("slicc_extension_call_type:$extension", $type);// let dispostion untrack this key so they can still manual dial
    }

    public static function untrack($extension, $type, $event)
    {
        $userGuid = User::guid($extension);

        RedisClient::connection()->del("slicc_extension_call_$type:" . $extension);

        RedisClient::connection()->del("slicc_call_extension_$type:" . $event->getKey('uniqueid'));

        RedisClient::connection()->del("slicc_user_call_$type:$userGuid");

        // RedisClient::connection()->del("slicc_$type_catched:$userGuid");

        // RedisClient::connection()->del("slicc_$type_request:$userGuid");
    }

    public static function pause($userGuid, $state)
    {
        if ($userGuid) {
            $extension = User::extension($userGuid);
            
            $type = Extension::callType($extension);

            $channels = RedisClient::connection()->smembers("slicc_bridge_channels_$type:$extension");

            foreach ($channels as $channel) {
                ActionHandler::monitor($channel, $state);
            }
        }
    }

    public static function appointment($userGuid, $callGuid, $lead, $start, $reason, $type = 'Appointment')
    {
        Logging::write('DATA_CALL[appointment]: Register new appointment: ' . $callGuid);

        self::push('Calls', [
            'id' => $callGuid,
            'new_with_id' => 1,
            'direction' => $type,
            'name' => 'SLI Phone appointment with ' . $lead['data']['first_name'] . ' ' . $lead['data']['last_name'],
            'parent_id' => $lead['data']['id'],
            'parent_type' => 'Leads',
            'status' => 'Force_Booked',
            'reminder_time' => '600',
            'duration_hours' => '0',
            'duration_minutes' => '30',
            'description' => 'SLI Phone appointment with ' . $lead['data']['first_name'] . ' ' . $lead['data']['last_name'] . ', booked by ' . User::name($userGuid),
            'notes_c' => $reason,
            'created_by' => $userGuid,
            'modified_user_id' => $userGuid,
            'update_modified_by' => false,
            'date_start' => $start->toDateTimeString()
        ]);

        return true;
    }

    public static function planner($userGuid, $callGuid, $lead, $start, $reason, $type = 'Callback')
    {
        Logging::write('DATA_CALL[planner]: Register new callback: ' . $callGuid);

        self::store($userGuid, $callGuid, $lead, $type, true, $start, null, 1, 'Planned');

        self::push('Calls', [
            'id' => $callGuid,
            'new_with_id' => 1,
            'is_this_callback' => 1,
            'direction' => $type,
            'name' => str_replace('_', ' ', $type) . ' to ' . $lead['data']['first_name'] . ' ' . $lead['data']['last_name'],
            'parent_id' => $lead['data']['id'],
            'parent_type' => 'Leads',
            'status' => 'Planned',
            'reminder_time' => '600',
            'duration_hours' => '0',
            'duration_minutes' => '30',
            'notes_c' => $reason,
            'created_by' => $userGuid,
            'modified_user_id' => $userGuid,
            'assigned_user_id' => $userGuid,
            'update_modified_by' => false,
            'date_start' => $start->toDateTimeString()
        ]);

        return true;
    }

    public static function store($userGuid, $callGuid, $lead, $type, $new = true, $startTime = null, $endTime = null, $callback = 0, $status = 'Not Held')
    {
        Logging::write('DATA_CALL[store]: Storing to redis: ' . $callGuid);

        if ($new) {
            $call = [
                'id' => $callGuid,
                'assigned_user_id' => $userGuid,
                'date_start' => $startTime ? $startTime->toDateTimeString() : Carbon::now('UTC')->toDateTimeString(),
                'date_end' => '',
                'is_this_callback' => $callback,
                'parent_id' => $lead['data']['id'],
                'status' => $status,
                'direction' => ucfirst($type),
                'description' => '',
                'date_entered' => Carbon::now('UTC')->toDateTimeString(),
                'date_modified' => Carbon::now('UTC')->toDateTimeString(),
                'name' => 'Call ' . ($type == 'Outbound' ? 'to ' : 'from ') . $lead['phoneNumber'],
                'call_duration_sec' => 0,
                'notes_c' => ''
            ];
        } else {
            $call = [
                'status' => 'Held',
                'call_duration_sec' => $startTime->diffInSeconds($endTime),
                'date_start' => $startTime->toDateTimeString(),
                'date_end' => $endTime->toDateTimeString()
            ];
        }

        Logging::write('DATA_CALL[register]: lead_id: ' . $lead['data']['id']);

        if (!$callback && $new) {
            RedisClient::connection()->zadd("slicc_user_calls:" . $userGuid, Carbon::parse($call['date_entered'])->timestamp, $callGuid);
        }

        RedisClient::connection()->zadd("slicc_user_leads:" . $userGuid, Carbon::parse($lead['data']['date_modified'])->timestamp, $lead['data']['id']);

        RedisClient::connection()->hmset("slicc_call_info:$callGuid", $call);
    }

    public static function register($userGuid, $callGuid, $lead, $type = 'Outbound')
    {
        Logging::write('DATA_CALL[register]: Register new call: ' . $callGuid);

        self::store($userGuid, $callGuid, $lead, $type);

        self::push('Calls', [
            'id' => $callGuid,
            'new_with_id' => 1,
            'status' => 'Not Held',
            'direction' => $type,
            'name' => 'Call ' . ($type == 'Outbound' ? 'to ' : 'from ') . $lead['phoneNumber'],
            'assigned_user_id' => $userGuid,
            'parent_id' => $lead['data']['id'],
            'parent_type' => 'Leads',
            'call_duration_sec' => '0',
            'duration_hours' => '0',
            'duration_minutes' => '0'
        ]);

        return true;
    }

    public static function update($userGuid, $callGuid, $lead, $type, $uniqueId, $startTime, $endTime)
    {
        Logging::write('DATA_CALL[update]: Updating a call: ' . $callGuid);

        self::store($userGuid, $callGuid, $lead, $type, false, $startTime, $endTime);

        self::push('Calls', [
            'id' => $callGuid,
            'status' => 'Held',
            'duration_hours' => $startTime->diffInHours($endTime),
            'duration_minutes' => $startTime->diffInMinutes($endTime),
            'call_duration_sec' => $startTime->diffInSeconds($endTime),
            'ast_unique_id_c' => $uniqueId,
            'date_start' => $startTime->toDateTimeString()
        ]);

        return true;
    }
}
