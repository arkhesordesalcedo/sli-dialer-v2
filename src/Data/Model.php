<?php

namespace SliCallCenter\Data;

use SliCallCenter\Helpers\Logging;
use SliCallCenter\Helpers\Validate;
use SliCallCenter\Connectors\SliCrmClient;

abstract class Model
{
    public static $pusher = 1;

	protected static function push($module, $data)
	{
		$context = new \ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'Data Saving...');
        $socket->connect(getenv('SLI_CALLCENTER_ZMQ_CONNECTION_' . self::$pusher));

        self::$pusher++;

        if (self::$pusher > 10) {
            self::$pusher = 1;
        }
        
        $message = [
        	'channel' => 'Queue',
        	'type' => 'save',
        	'data' => [$module, $data]
        ];
    
        $socket->send(Validate::outgoing($message), \ZMQ::MODE_DONTWAIT);

        return true;
	}
}
