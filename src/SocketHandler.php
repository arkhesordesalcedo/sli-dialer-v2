<?php

namespace SliCallCenter;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use SliCallCenter\Helpers\Auth;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Helpers\Validate;
use SliCallCenter\Channels\Queue;

class SocketHandler implements MessageComponentInterface
{
    public function onOpen(ConnectionInterface $conn) {
        Logging::write('SOCKET_HANDLER[onOpen]: New connection! (' . $conn->resourceId . ') by ' . $conn->remoteAddress);
    }

    public function onMessage(ConnectionInterface $conn, $raw_message) {
        Auth::allowed($conn) ? $this->onDataReceived($conn, $raw_message) : Auth::attempt($conn, Validate::incoming($raw_message));
    }

    public function onClose(ConnectionInterface $conn) {
        Auth::deauthorize($conn);

        Logging::write('SOCKET_HANDLER[onClose]: Connection ' . $conn->resourceId . ' has disconnected.');
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        Auth::deauthorize($conn);

        Logging::write('SOCKET_HANDLER[onError]: An error has occurred: '. $e->getMessage());
    }

    public function onDataReceived(ConnectionInterface $conn, $message)
    {   
        if(!$validatedMessage = Validate::incoming($message)) {
            return false;
        }

        return Channels::verify($validatedMessage) ? Channels::trigger($conn, $validatedMessage) : false;
    }
}