<?php

namespace SliCallCenter\Channels;

use Carbon\Carbon;
use Ratchet\ConnectionInterface;
use SliCallCenter\Connectors\RedisClient;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Helpers\Message;
use SliCallCenter\Data\User;
use SliCallCenter\Data\Call;
use SliCallCenter\Data\Lead;
use SliCallCenter\Data\Extension;
use SliCallCenter\Pbx\ActionHandler;

class iApplication extends Message
{
    public function __construct(ConnectionInterface $conn, $message)
    {
        parent::__construct($conn, $message);
    }
    
    public function getCallerState()
    {
        // TODO: refactor this shit
        if ($userGuid = $this->get('userGuid')) {
            $extension = User::extension($userGuid);

            $type = Extension::callType($extension);

            if (User::leads($userGuid, $type)) {
                if ($leadGuid = RedisClient::connection()->get("slicc_user_lead_$type:$userGuid")) {
                    $lead = Lead::find($leadGuid);

                    $feedback['data']['type'] = $type;

                    foreach ($lead as $key => $value) {
                        $feedback['data']['lead'][$key] = htmlentities($value);
                    }

                    if (User::inbound($this, $lead, $userGuid, $feedback)) {
                        return true;
                    }

                    if (User::catched($userGuid)) {
                        $feedback['state'] = 'incomingCallRequest';

                        $feedback = Lead::inbound($lead['phone_mobile'], $feedback);
                    } else {
                        if (User::calls($userGuid, $type)) {
                            $feedback['state'] = 'onTheCall';
                        } else {
                            $feedback['state'] = 'disposition';
                        }
                    }
                }
            } else {
                $feedback['state'] = 'noCalls';
            }

            $this->reply($feedback);
        }
        
        return false;
    }

    public function dispositionCall()
    {
        if ($this->get('userGuid') && $this->get('dispositionValue') && $this->get('leadGuid') && $this->get('type')) {
            Logging::write('CHANNEL_iAPPLICATION[dispositionCall]: Disposing lead: ' . $this->get('leadGuid'));

            return Lead::dispose($this);
        }

        return false;
    }

    public function pauseCallRecording()
    {
        Call::pause($this->get('userGuid'), 1);
    }

    public function resumeCallRecording()
    {
        Call::pause($this->get('userGuid'), 0);
    }

    public function getCall()
    {
        Extension::endCall($this->get('userGuid'), User::extension($this->get('userGuid')));
        
        Logging::write('CHANNEL_iAPPLICATION[getCall]: New call request: ' . $this->get('userGuid'));

        // TODO: refactor this shit
        if ($callGuid = User::callback($this->get('userGuid'))) {
            if ($lead = Lead::transform(Call::lead($callGuid))) {
                Logging::write('CHANNEL_iAPPLICATION[getCall]: Callback lead due: ' . $callGuid);

                RedisClient::connection()->zrem("slicc_user_callbacks:" . $this->get('userGuid'), $callGuid);

                return User::call($this, $lead, 'callback', $callGuid);
            }
        }

        if($lead = $this->assert($this->get('userGuid'))) {
            if (isset($lead['phoneNumber'])) {
                Logging::write('CHANNEL_iAPPLICATION[getCall]: Requested lead: ' . print_r($lead['phoneNumber'], true));

                return User::call($this, $lead);
            }
        }

        return false;
    }

    /**
     * Ends all calls
     * 
     * @return boolean
     */
    public function endCall()
    {
        if ($userGuid = $this->get('userGuid')) {
            Logging::write('CHANNEL_iAPPLICATION[endCall]: End call request: ' . $this->get('userGuid'));

            $extension = User::extension($userGuid);

            return Extension::endCall($extension);
        }
        
        return false;
    }

    // public function recentCalls()
    // {
    //     if ($userGuid = $this->get('userGuid')) {
    //         if ($calls = User::recentCalls($userGuid, $this->get('callbacks'))) {
    //             $this->reply(['data' => $calls], 'Calls');
    //         }
    //     }
        
    //     return false;
    // }

    // public function recentLeads()
    // {
    //     if ($userGuid = $this->get('userGuid')) {
    //         if ($leads = User::recentLeads($userGuid)) {
    //             $this->reply(['data' => $leads], 'Leads');
    //         }
    //     }
        
    //     return false;
    // }

    public function updateLead()
    {
        if ($userGuid = $this->get('userGuid')) {
            $encoded = $this->get('data');

            $decoded = [];

            foreach ($encoded as $field => $value) {
                if ($field == 'new_with_id' || $field == 'status') {
                    continue;
                }

                if ($field == 'insurance_amount') {
                    $field = 'coverage_requested_c';
                }

                $decoded[$field] = base64_decode($value);

                if ($field == 'gender_c') {
                    $decoded[$field] = $decoded[$field] == 'm' ? 'male' : 'female';
                }
            }

            $decoded['modified_user_id'] =  $userGuid;
            $decoded['update_modified_by'] = false;

            // Logging::write('CHANNEL_iAPPLICATION[updateLead]: lead decoded from iApp update: ' . print_r($decoded, true));
            
            Lead::modify($decoded);
        }
        
        return false;
    }

    private function assert($userGuid)
    {
        // check if user is allowed or has active call before searching for lead
        $lead = !User::allowed($userGuid) ?: User::calls($userGuid) ?: Lead::provide($userGuid);
        
        // Logging::write('CHANNEL_iAPPLICATION[assert]: New call received: ' . print_r($lead, true));
        // return lead if data is available
        return isset($lead['data']) ? $lead : false;
    }
}

