<?php

namespace SliCallCenter\Channels;

use SliCallCenter\Helpers\Logging;
use SliCallCenter\Connectors\SliCrmClient;

class Queue
{
	public static function save($module, $data)
	{
		if ($crm = (new SliCrmClient)->connect()) {
			$result = $crm->save($module, $data);

        	Logging::write('CHANNEL_QUEUE[save]: Saving data to CRM result: ' . print_r($result, true));

            return true;
    	}

    	Logging::write('CHANNEL_QUEUE[save]: Failed saving data to CRM result: ' . $data['id']);

    	return false;
	}
}