<?php

namespace SliCallCenter\Channels;

use Ratchet\ConnectionInterface;
use SliCallCenter\Helpers\Message;
use SliCallCenter\Data\Lead as DataLead;

class Lead extends Message
{
	public function __construct(ConnectionInterface $conn, $message)
    {
        parent::__construct($conn, $message);
    }

    public function stats()
    {
    	$stats = [];
    	
    	return $this->reply($stats);
    }
}