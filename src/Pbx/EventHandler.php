<?php

namespace SliCallCenter\Pbx;

use SliCallCenter\Data\Call;
use SliCallCenter\Data\Extension;
use SliCallCenter\Helpers\Logging;
use PAMI\Message\Event\EventMessage;
use SliCallCenter\Connectors\RedisClient;

class EventHandler
{
    public static $events = [
        'Bridge', 
        'Hangup', 
        'Newexten',
        'VarSet', 
        'Dial', 
        'ExtensionStatus'
    ];

    private static function allowed($channel)
    {
        $alloweExtensions = Extension::all();

        foreach ($alloweExtensions as $sipNumber) {
            $needles = array(
                'SIP/'. $sipNumber .'-',
                'Local/'. $sipNumber .'@',
            );
            
            foreach ($needles as $needle) {
                if (strstr($channel, $needle)) {
                    return $sipNumber;
                }
            }
        }
        
        return false;
    }

    /**
     * Triger the event when an extension's state has beend updated and
     * sets redis key to the new extension state
     *
     * -1 = Extension not found
     * 0 = Idle
     * 1 = In Use
     * 2 = Busy
     * 4 = Unavailable
     * 8 = Ringing
     * 16 = On Hold
     *    
     * @param  EventMessage $event
     * @return boolean
     */
    public static function event_ExtensionStatus(EventMessage $event)
    {
        Logging::write('PBX_EVENT_HANDLER[event_Dial]: New extension status event: ' . print_r($event->getKey('hint'), true), 'amiEvents');
        
        Extension::status($event->getKey('exten'), $event->getKey('status'));

        return true;
    }

    public static function event_Dial(EventMessage $event)
    {
        Logging::write('PBX_EVENT_HANDLER[event_Dial]: New dial event: ' . print_r($event->getKeys(), true), $event->getName());

        $extension = self::allowed($event->getKey('channel'));

        if ($event->getKey('subevent') == 'Begin' && $extension) { // this is for outbound dial event
            Call::make($event, $extension);

            return true;
        }

        return false;
    }

    public static function event_Bridge(EventMessage $event)
    {
        Logging::write('PBX_EVENT_HANDLER[event_Bridge]: New bridge event: ' . print_r($event->getKeys(), true), $event->getName());

        $extension = self::allowed($event->getKey('channel1')) ?: self::allowed($event->getKey('channel2'));

        if ($extension) {
            Call::observe($event, $extension);

            return true;
        }

        return false;
    }

    public static function event_Hangup(EventMessage $event)
    {
        Logging::write('PBX_EVENT_HANDLER[event_Hangup]: New hangup event: ' . print_r($event->getKeys(), true), $event->getName());

        $extension = self::allowed($event->getKey('channel'));

        if ($extension) {
            Call::hangup($event, $extension);

            return true;
        }

        return false;
    }

    // public static function event_Newstate(EventMessage $event)
    // {
        
    //     Logging::write('PBX_EVENT_HANDLER[event_Newstate]: New state event: ' . print_r($event->getKeys(), true), $event->getName());

    //     return true;
    // }

    /**
     * Triggers on the event
     * 
     * @param  EventMessage
     * 
     * @return boolean
     */
    // public static function event_Newchannel(EventMessage $event)
    // {
    //     if (self::allowed($event->getKey('channel'))) {
    //         Logging::write('PBX_EVENT_HANDLER[event_Newchannel]: New channel event: ' . print_r($event->getKeys(), true), $event->getName());

    //         return true;
    //     }

    //     return false;
    // }

    public static function event_Newexten(EventMessage $event)
    {
        // Logging::write('PBX_EVENT_HANDLER[event_Newexten]: New exten event: ' .  print_r($event->getKey('appdata'), true), 'amiEvents');

        // if ($event->getKey('context') == 'custom-get-did-from-sip') {
        //     // RedisClient::connection()->transaction(function($tx) use ($event) {
        //     //     $tx->multi();

        //     //     $tx->set("slicc_new_exten_channel:" . $event->getKey('channel'), $event->getKey('appdata'));

        //     //     $tx->expire("slicc_new_exten_channel:" . $event->getKey('channel'), 300);
        //     // });

        //     RedisClient::connection()->set("slicc_new_exten_channel:" . $event->getKey('channel'), $event->getKey('appdata'));

        //     RedisClient::connection()->expire("slicc_new_exten_channel:" . $event->getKey('channel'), 300);

        //     return true;
        // }
        
        return true;
    }

    public static function event_VarSet(EventMessage $event)
    {
        
        // Logging::write('PBX_EVENT_HANDLER[event_VarSet]: New varset event: ' . print_r($event->getKey('channel'), true), 'amiEvents');

        return true;

        // return Call::variables($event);
    }
}