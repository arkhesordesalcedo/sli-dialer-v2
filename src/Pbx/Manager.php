<?php

namespace SliCallCenter\Pbx;

use SliCallCenter\Helpers\Logging;
use PAMI\Message\Action\ActionMessage;
use PAMI\Client\Exception\ClientException;
use PAMI\Client\Impl\ClientImpl as Asterisk;

class Manager
{
    public static $context = 'from-internal';

    public static $connection = null;

    public static function getSettings()
    {
    	return [
	        'scheme' => 'tcp://',
	        'host' => getenv('AMI_HOST'),
	        'port' => getenv('AMI_PORT'),
	        'username' => getenv('AMI_USERNAME'),
	        'secret' => getenv('AMI_SECRET'),
	        'connect_timeout' => getenv('AMI_CONNECT_TIMEOUT'),
	        'read_timeout' => getenv('AMI_READ_TIMEOUT')
	    ];
    }
    
    public static function connect($automatically = false)
    {
        self::disconnect();
        
        try {
            self::$connection = new Asterisk(self::getSettings());

            if ($automatically) {
            	self::$connection->open();
            }
        } catch (\Exception $e) {
            Logging::write('PBX_MANAGER_ERROR[connect]: Connecting to AMI has failed: ' . $e->getMessage());

            return false;
        }
        
	    return true;
    }

    public static function disconnect()
    {
        if (self::$connection) {
        	try {
        		self::$connection->close();
        	} catch (\Exception $e) {
        		Logging::write('PBX_MANAGER_ERROR[disconnect]: Closing connection to AMI has failed: ' . $e->getMessage());
        	}
            
        }
         
        return true;
    }

    public static function send(ActionMessage $message)
    {
        try {
            return self::$connection->send($message);
        }  catch (ClientException $e) {
            if ($e->getMessage() != 'Read timeout') {
                Logging::write('PBX_MANAGER_ERROR[send]: AMI error: ' . $e->getMessage());

                return false;
            }
        }
           
        return true;
    }
}