<?php

namespace SliCallCenter\Pbx;

use Carbon\Carbon;
use PAMI\Message\Action as Asterisk;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Helpers\Validate;
use SliCallCenter\Helpers\Operation;

class ActionHandler
{
    public static function call($extension, $phone)
    {
        // create a call log
        $phone = Operation::mode() == 'D' ? Operation::phone() : $phone;
        // $phone = '902';

        Manager::connect(true);
         
        $new = new Asterisk\OriginateAction('SIP/' . Validate::number($extension));
        $new->setContext(Manager::$context);
        $new->setPriority('1');
        $new->setExtension(Validate::number($phone));
        $new->setAsync(true);
         
        if ($result = Manager::send($new)) {
            return true;
        }
        
        Logging::write('PBX_ACTION_HANDLER_ERROR[call]: Call originating in AMI has failed.');

        return false;
    }

    public static function monitor($channel, $state)
    {
        try {
            $monitor = new Asterisk\MixMonitorMuteAction($channel, $state, 'both');

            Manager::connect(true);

            if ($result = Manager::send($monitor)) {
                Logging::write('PBX_ACTION_HANDLER_ERROR[monitor]: Channel monitor results: ' . print_r($result, true));

                return true;
            }
        } catch (\Exception $e) {
           Logging::pushToFile('PBX_ACTION_HANDLER_ERROR[monitor]: '.$e->getMessage());
        }
    }
}