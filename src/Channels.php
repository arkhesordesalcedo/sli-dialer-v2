<?php

namespace SliCallCenter;

use Ratchet\ConnectionInterface;
use SliCallCenter\Helpers\Logging;

class Channels
{
    public static function trigger(ConnectionInterface $conn, array $message)
    {
        $class  = '\\SliCallCenter\\Channels\\' . $message['channel'];

        $method = $message['action'];
        
        if (self::exists($class, $method)) {
            try {
                $channel = new $class($conn, $message);

                return $channel->get('userGuid') ? $channel->$method() : false;
            } catch (\Exception $e) {    
                Logging::write('CHANNELS_ERROR[trigger]: Channel\'s action invocation failed. Error:' . $e->getMessage());
            }
        }
        
        return false;
    }

    public static function verify($message)
    {
        return isset($message['action']) && isset($message['channel']) ? true : false;
    }
    
    public static function exists($class, $method)
    {
        return class_exists($class) && method_exists($class, $method);
    }
}