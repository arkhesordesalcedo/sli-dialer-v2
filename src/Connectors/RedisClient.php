<?php

namespace SliCallCenter\Connectors;

use Predis\Client;

/**
 * Redis client class
 *
 * TODO: implement cas on transactions with multiple keys
 *
 * @author  Arkhe <arkhe@isi.ca>
 */
class RedisClient
{
	protected static $connection = null;

	protected static function connect()
	{
		self::$connection = new Client(
			getenv('REDIS_CONNECTION'),
			[
				'parameters' => [
					'password' => getenv('REDIS_PASSWORD')
				]
			]
		);

		return self::$connection;
	}

	public static function connection()
	{
		if (self::$connection) {
			return self::$connection;
		}

		return self::connect();
	}
}