<?php

namespace SliCallCenter\Connectors;

use SliCallCenter\Helpers\Logging;

class SliCrmClient
{    
    private $connection = null;

    public function __construct()
    {
        $credentials = $this->credentials();

        $this->connection = new SugarWrapper;

        $this->connection->setUrl($credentials['url']);
        $this->connection->setUsername($credentials['user']);
        $this->connection->setPassword($credentials['password']);
    }

    private function credentials()
    {
        return [
            'user' => getenv('SLI_CRM_USERNAME'),
            'password' => getenv('SLI_CRM_PASSWORD'),
            'url' => getenv('SLI_CRM_URL')
        ];
    }
    
    public function connect()
    {
        $this->connection->connect();

        if ($err = $this->connection->get_error()) {
            Logging::write('CONNECTOR_SLI_CRM_CLIENT_ERROR[connect]: Constructor error: ' . print_r($err, true));

            return false;
        }
        
        return $this;
    }
    
    public function save($module, $data)
    {
        if ($this->connection) {
            $result = $this->connection->set($module, $this->map($data));
            
            return isset($result['id']) ? $result['id'] : 'Error';
        }

        return false;
    }

    protected function map($data)
    {
        $newData = [];

        foreach ($data as $key => $value) {
            $newData[] = array(
                'name'  => $key,
                'value' => $value,
            );
        }

        return $newData;
    }
}
