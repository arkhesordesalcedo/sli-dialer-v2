<?php

namespace SliCallCenter\Helpers;

use Carbon\Carbon;
use SliCallCenter\Connectors\RedisClient;

class Timezone
{
	public static function allowed($state)
	{
		$time = (int) Carbon::now(RedisClient::connection()->get("slicc_states_timezone:$state"))->format('Gi');

		if ($time > Operation::open() && $time < Operation::close()) {

			return true;
		}

		return false;
	}

	public static function get($state)
	{
		return RedisClient::connection()->get('slicc_states_timezone:' . str_replace(' ', '_', $state));
	}
}