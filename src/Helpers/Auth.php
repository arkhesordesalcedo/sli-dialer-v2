<?php

namespace SliCallCenter\Helpers;

use Ratchet\ConnectionInterface;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Data\Connection;
use SliCallCenter\Data\User;

class Auth
{    
    public static function allowed(ConnectionInterface $conn)
    {
        return Connection::allowed($conn);
    }

    public static function attempt(ConnectionInterface $conn, array $message)
    {
        if (Validate::auth($message)) {

            if (self::compare($message['pass'][0], $message['pass'][1])) {

                Connection::authorize($conn);

                User::authorize($conn, $message['pass'][0]);

                $conn->send(json_encode(
                    array(
                        'channel' => 'authStatus',
                        'type'    => 'passStatus',
                        'status'  => 'granted'
                    )
                ));

                Logging::write('HELPERS_AUTH_SUCCESS[attempt]: Connection '. $conn->resourceId .' was authorized.');

                return true;
            }

        }

        return self::detach($conn);
    }

    public static function deauthorize(ConnectionInterface $conn, $resouceId = null)
    {
        Connection::remove($conn, $resouceId);

        Logging::write('HELPERS_AUTH_SUCCESS[deauthorize]: Connection '. $resouceId ?: $conn->resourceId .' was detached from Auth.');
    }

    private static function compare($username, $password)
    {
        return self::decrypt($username, $password) == User::getHashEncoded($username);
    }

    private static function decrypt($username, $password)
    {
        return base64_encode(crypt(strtolower($password), User::getHash($username)));
    }

    private static function detach(ConnectionInterface $conn)
    {
        $conn->send(json_encode(
            array(
                'channel' => 'authStatus',
                'type'    => 'passStatus',
                'status'  => 'denied',
                'statusError'  => 'AUTH_FAILED: You are not authorized for the action.',
            )
        ));

        $conn->close();

        Logging::write('HELPERS_AUTH_SUCCESS[detach]: A connection was dropped due to unauthorized activity.');
        
        return false;
    }
}