<?php

namespace SliCallCenter\Helpers;

/**
 * Class to handle Logging
 *
 * @author  Daniel
 * @author  Arkhe <arkhe@isi.ca>
 *
 * @company SLI
 */
class Logging
{
    public static function write($message, $eventName = null)
    {
        $t = microtime(true);
        
        $microtime = sprintf("%06d",($t - floor($t)) * 1000000);

        $message = (new \DateTime(date('Y-m-d H:i:s.' . $microtime, $t)))->format("Y-m-d H:i:s.u") . ': ' . $message;

        $path = getenv('LOG_FILE');

        if ($eventName) {
        	$path = getenv('AMI_FOLDER') . $eventName . '.log';
        }
        
        shell_exec('echo "'. addslashes($message) .'" >> ' . $path);
        
        return true;
    }
}