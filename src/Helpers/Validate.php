<?php

namespace SliCallCenter\Helpers;

use Ratchet\ConnectionInterface;

class Validate
{
    public static function incoming($message)
    {
        // TODO: Figure why it so...  it send a stack of delayed messages...
        if (strstr($message, '}{')) {
            $chuncks = explode('}{', $message);
            
            $message = '{' . $chuncks[1];
            
            if (count($chuncks) > 2) {
                $message = $message . '}';
            }
        }
        // TODO: EOF
        
        if ($message = self::getJson($message)) {
            if (self::basics($message)) {
                return $message;
            }
        }
        
        return array();
    }

    public static function outgoing(array $message)
    {
        if (self::basics($message)) {
            return json_encode($message);
        }
        
        return null;
    }
    
    public static function failed(ConnectionInterface $conn)
    {
        $conn->send('Bye');

        $conn->close();
        
        Logging::pushToFile('HELPERS_VALIDATE_FAILED[failed]: Closing connection for IP: ' . $conn->remoteAddress);
        
        return false;
    }
    
    public static function basics(array $message)
    {
        if (isset($message['channel']) 
            || isset($message['type']) 
            || $message['channel']
            || $message['type']) {
                return true;
        }
        
        return false;
    }

    public static function auth(array $message)
    {
        if (isset($message['type']) 
            && $message['type'] == 'authRequest' 
            && isset($message['pass']) 
            && isset($message['pass'][0]) 
            && isset($message['pass'][1]) 
            && isset($message['profile'])) {
                return true;
        }

        return false;
    }

    public static function getJson($raw_entry)
    {
        $message = @json_decode($raw_entry, true);
        
        if (!is_array($message) || !$message ) {
            return array();
        }
        
        $message['raw'] = $raw_entry;
        
        return $message;
    }

    public static function number($phone_number)
    {
        return preg_replace('/\D/', null, $phone_number);
    }
}