<?php

namespace SliCallCenter\Helpers;

use Carbon\Carbon;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Connectors\RedisClient;

class Utils
{
	public static function decode($encoded_row)
	{
		$decoded_row = array();
    
        foreach ($encoded_row as $field => $value) {
            $decoded_row[$field] = base64_decode($value);
        }
    
        return $decoded_row;
	}

	public static function generateGUID()
	{
		if (function_exists('com_create_guid')){
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = md5(uniqid(rand(), true));
            $hyphen = chr(45);// "-"
            //$uuid = chr(123)// "{"
            $uuid = substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12);
            //.chr(125);// "}"
            
            return $uuid;
        }
	}

    public static function replace($string, $char = ' ')
    {
        return str_replace($char, '_', $string);
    }

    public static function transform($ids, $date, $key)
    {
        $result = [];

        if ($ids) {
            foreach ($ids as $id) {
                $holders = RedisClient::connection()->hgetall($key . $id);

                $temp = [];

                foreach ($holders as $field => $value) {
                    // if (in_array($field, $date)) {
                    //     $diff = Carbon::parse($value)->diffInMinutes(Carbon::now('UTC'));
                        
                    //     $value = Carbon::now('UTC')->subMinutes($diff)->diffForHumans();
                    // }

                    $temp['fetched_row'][$field] = base64_encode($value);
                }

                $result[] = $temp;
            }

            
        }
        
        return $result;
    }
}