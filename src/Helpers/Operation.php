<?php

namespace SliCallCenter\Helpers;

use Carbon\Carbon;
use SliCallCenter\Connectors\RedisClient;

class Operation
{
	public static function mode()
	{
		return RedisClient::connection()->get('slicc_mode');
	}

	public static function open()
	{
		return (int) RedisClient::connection()->get('slicc_operation_start');
	}

	public static function close()
	{
		return (int) RedisClient::connection()->get('slicc_operation_end');
	}

	public static function phone()
	{
		return (int) RedisClient::connection()->get('slicc_dev_phone');
	}

	public static function sequence()
	{
		$callingFrequency = RedisClient::connection()->hgetall('slicc_lead_calling_frequency');

		ksort($callingFrequency);

		return $callingFrequency;
	}

	public static function calculate($timestamp)
	{
		$time = Carbon::createFromTimeStamp($timestamp, 'America/Toronto');

		return $time->between(self::time('open'), self::time('close')) ? $time->timestamp : self::offset($timestamp);
	}

	public static function offset($timestamp)
	{
		$offset = self::time('close')->diffInMinutes(self::time('open')->addDay());

		return Carbon::createFromTimeStamp($timestamp, 'America/Toronto')->addMinutes($offset)->timestamp;
	}

	private static function split($time)
	{
		return [
			strlen($time) > 3 ? substr($time, 0, 2) : substr('0' . $time, 0, 2),
			substr($time, -2)
		];
	}

	public static function time($boundary)
	{
		$time = self::split(self::{$boundary}());

		$t = Carbon::now('America/Toronto');

		$t->hour = $time[0];

		$t->minute = $time[1];

		return $t;
	}
}