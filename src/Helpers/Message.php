<?php

namespace SliCallCenter\Helpers;

use Ratchet\ConnectionInterface;

class Message
{
    protected $data = [];
    
    protected $connection = null;

    public function __construct(ConnectionInterface $conn, $message)
    {
        $this->data = $message;

        $this->connection = $conn;
    }
    
    public function get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }
    
    public function reply(array $payload, $type = null, $action = null)
    {
        if ($this->connection) {
            $body = array(
                'channel' => $this->get('channel'),
                'action'  => $action ? $action : $this->get('action'),
                'type'    => $type ? $type : 'replyTo_' . $this->get('type')
            );
            
            $body = array_merge($body, $payload);
            
            $this->connection->send(Validate::outgoing($body));
            
            return true;
        }

        return false;
    }
}