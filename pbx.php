<?php

require dirname(__FILE__) . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

use PAMI\Message\Event\EventMessage;
use SliCallCenter\Helpers\Logging;
use SliCallCenter\Pbx\Manager;
use SliCallCenter\Pbx\EventHandler;

try {
    $pami = new Manager;

    $pami::connect();
    
    $pami::$connection->registerEventListener(
        function(EventMessage $event) {
            if (method_exists(EventHandler::class, $method = 'event_' . $event->getName())) {
                EventHandler::$method($event);
            }
        },
        function(EventMessage $event) {
            if (in_array($event->getName(), EventHandler::$events)) {
                return true;
            }

            return false;
        }
    );
    
    $pami::$connection->open();

    while (true) {
        usleep(1000);
         
        $pami::$connection->process();
    }
    
    $pami->disconnect();
    
} catch (Exception $e) {
    Logging::write('PBX_DEAMON: Deamon has failed. Error: ' . $e->getMessage());
}