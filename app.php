<?php

require dirname(__FILE__) . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use SliCallCenter\SocketHandler;

$loop = React\EventLoop\Factory::create();

$handler = new SocketHandler();

// Set up our WebSocket server for clients wanting real-time updates
$webSocket = new React\Socket\Server($loop);
$webSocket->listen(getenv('SLI_CALLCENTER_WEBSOCKET_PORT'), '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
$webServer = new IoServer(new HttpServer(new WsServer($handler)), $webSocket);

$loop->run();
