<?php

require dirname(__FILE__) . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

use Carbon\Carbon;
use SliCallCenter\Connectors\RedisClient;
use Predis\Collection\Iterator;
use SliCallCenter\Data\Lead;
use SliCallCenter\Helpers\Operation;


// $interval = Carbon::now('America/Toronto')->addMinutes(150);

// $expires = Carbon::createFromTimeStamp(Operation::calculate($interval->timestamp), 'America/Toronto');

// if (Carbon::now('America/Toronto')->timestamp > $expires->timestamp) {
// 	echo 'purging';
// } else {
// 	echo 'pending....' . PHP_EOL;
// }

// die();

// $states = array(
//     'Ontario'                   => 'America/Toronto',
//     'ON'                        => 'America/Toronto',
//     'Quebec'                    => 'America/Montreal',
//     'QC'                        => 'America/Montreal',
//     'Nova Scotia'               => 'America/Halifax',
//     'NS'                        => 'America/Halifax',
//     'New Brunswick'             => 'America/Halifax',
//     'NB'                        => 'America/Halifax',
//     'Manitoba'                  => 'America/Winnipeg',
//     'MB'                        => 'America/Winnipeg',
//     'British Columbia'          => 'America/Vancouver',
//     'BC'                        => 'America/Vancouver',
//     'Prince Edward Island'      => 'America/Halifax',
//     'PE'                        => 'America/Halifax',
//     'Saskatchewan'              => 'America/Regina',
//     'SK'                        => 'America/Regina',
//     'Alberta'                   => 'America/Edmonton',
//     'AB'                        => 'America/Edmonton',
//     'Newfoundland and Labrador' => 'America/Halifax',
//     'NL'                        => 'America/Halifax',
//     'Northwest Territories'     => 'America/Yellowknife',
//     'NT'                        => 'America/Yellowknife',
//     'Yukon'                     => 'America/Whitehorse',
//     'YT'                        => 'America/Whitehorse',
//     'Nunavut'                   => 'America/Iqaluit',
//     'NU'                        => 'America/Iqaluit',
// );
// $ctr = 0;

// foreach ($states as $state => $tz) {
// 	foreach (new Iterator\SetKey(RedisClient::connection(), 'slicc_lead_callable_by_state:' . str_replace(' ', '_', $state)) as $leadGuid) {
// 		if (Lead::age($leadGuid) > 30) {
// 			echo $leadGuid . ': ' . Lead::set($leadGuid, 'total_dials', 30) . PHP_EOL;
// 		}
// 	}
// }

// echo $ctr . PHP_EOL;

// die();

// $ctr = 0;

// foreach (new Iterator\SetKey(RedisClient::connection(), 'slicc_lead_do_not_dial') as $leadGuid) {
// 	// $status = RedisClient::connection()->hget("slicc_lead_info:$leadGuid", 'status');

// 	if (!RedisClient::connection()->exists("slicc_lead_info:$leadGuid")) {
// 		echo RedisClient::connection()->srem('slicc_lead_do_not_dial', $leadGuid) . PHP_EOL;

// 		$ctr++;
// 	}
// 	// if ($status == 'Voicemail') {
// 	// 	echo RedisClient::connection()->srem('slicc_lead_do_not_dial', $leadGuid) . PHP_EOL;

// 	// 	$ctr++;
// 	// }
// }

// echo $ctr . PHP_EOL;

// die();

$start = Carbon::now('America/Toronto')->addHour(1)->timestamp;

$end = Carbon::now('America/Toronto')->addHour(5)->timestamp;

// var_dump($now); die();

$ctr = 0;

foreach (new Iterator\SortedSetKey(RedisClient::connection(), 'slicc_lead_waiting_timer') as $leadGuid => $timestamp) {
    if ($timestamp > $start && $timestamp < $end) {
    	$ctr++;

    	echo $leadGuid . PHP_EOL;

        RedisClient::connection()->zrem("slicc_lead_waiting_timer", $leadGuid);

        RedisClient::connection()->srem("slicc_lead_waiting",  $leadGuid);
    }
}

echo $ctr . PHP_EOL;

die();

// $callTypes = ['inbound', 'outbound', 'manual', 'callback', 'incoming'];

// $extension = '325';

// $userGuid = '8f15024f-f38f-c4e4-2872-58a76bb1dd32';


// foreach ($callTypes as $type) {
// 	// TODO: figure out a way to update calls that AMI fails to track
// 	echo RedisClient::connection()->del("slicc_extension_call_$type:" . $userGuid);

// 	echo RedisClient::connection()->del("slicc_extension_call_type:" . $extension);

// 	echo RedisClient::connection()->del("slicc_user_call_$type:" . $userGuid);

// 	echo RedisClient::connection()->del("slicc_inbound_catched:$userGuid");
// }

// var_dump('ok');
